import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import NewArticle from '../views/NewArticle.vue'
import Article from '../components/Article.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  { 
    // dynamic route
    path: '/article/:id',
    component: Article,
    // get props dynamically by route param
    props: (route) => store.state.articles.find((x) => x.id == route.params.id)
  },
  {
    path: '/new',
    name: 'Add new article',
    component: NewArticle
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
