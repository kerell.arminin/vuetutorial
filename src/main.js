import Vue from 'vue'
import App from './App.vue'
import CustomInput from './components/CustomInput.vue'
import router from './router'
import store from './store'
import {Types} from './store/types'

Vue.config.productionTip = false;

Vue.component('CustomInput', CustomInput);

store.dispatch(Types.actions.ARTICLES_LOAD);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
